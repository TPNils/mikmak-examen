<?php $GLOBALS['nav.activePage']="product"; ?>
@extends('layout.master')

@section('head')
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
@endsection

@section('title')
Product
@endsection

@section('main')
<table style="border: none; width: 100%">
    <thead style="text-align: center">
        <tr>
            <td><h1>Producten</h1></td>
            <td width="8%"><a class="btn btn-primary" href="{{ route('product.create') }}">Nieuw</a></td>
        </tr>
    </thead>
</table>
<table class="table-classic">
    <thead>
        <tr>
            <td width="60%">Naam</td>
            <td width="10%">Prijs</td>
            <td width="10%">Select</td>
            <td width="10%">Update</td>
            <td width="10%">Delete</td>
        </tr>
    </thead>
    <tbody>
    @foreach($products as $key => $value)
        <tr>
            <td>{{ $value->Name }}</td>
            <td>{{ $value->Price }}</td>
            <td style="text-align: center"><a href="{{ route('product.show', $value->Id) }}" class="btn btn-primary">Select</a></td>
            <td style="text-align: center"><a href="{{ route('product.edit', $value->Id) }}" class="btn btn-primary">Update</a></td>
            <td style="text-align: center">
            {!! Form::open([
                'route' => ['product.destroy', $value->Id],
                'method' => 'DELETE'
            ]) !!}                    
            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
            {{ Form::close() }}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
{{ $products->links() }}
@endsection