<?php $GLOBALS['nav.activePage']="product"; ?>
@extends('layout.crud')

@section('title')
Nieuw product
@endsection

@section('side-view')
@include('product.layout.aside')
@endsection()

@section('content')
{!! Form::open([
    'route' => 'product.store'
]) !!}

<table class="table" style="border: none;">
    <thead style="text-align: center">
        <tr>
            <td>Product</td>
            <td width="8%">{!! Form::submit('Create', ['class' => 'btn btn-primary']) !!}</a></td>
            <td width="8%"><a class="btn btn-primary" href="{{ route('product.index') }}">Cancel</a></td>
        </tr>
    </thead>
</table>

<div class="form-group">
    {!! Form::label('name', 'Naam:', ['class' => 'control-label']) !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('price', 'Prijs:', ['class' => 'control-label']) !!}
    {!! Form::text('price', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('shippingCost', 'Verzendkosten:', ['class' => 'control-label']) !!}
    {!! Form::text('shippingCost', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('thumbnail', 'Miniatuur:', ['class' => 'control-label']) !!}
    {!! Form::text('thumbnail', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('image', 'Afbeelding:', ['class' => 'control-label']) !!}
    {!! Form::text('image', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('discountPercentage', 'Aanbiedingspercent:', ['class' => 'control-label']) !!}
    {!! Form::text('discountPercentage', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('categoryName', 'Categorie:', ['class' => 'control-label']) !!}
    {!! Form::text('categoryName', null, ['class' => 'form-control']) !!}
</div>

{!! Form::close() !!}
@endsection()