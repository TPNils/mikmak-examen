<?php $GLOBALS['nav.activePage']="product"; ?>
@extends('layout.crud')

@section('title')
{{ $product->Name }}
@endsection

@section('side-view')
@include('product.layout.aside')
@endsection()

@section('content')
<table class="table" style="border: none;">
    <thead style="text-align: center">
        <tr>
            <td>Product</td>
            <td width="8%"><a class="btn btn-primary" href="{{ route('product.edit', $product->Id) }}">Update</a></td>
            <td width="8%"><a class="btn btn-primary" href="{{ route('product.create') }}">Nieuw</a></td>
            <td width="8%">
                {{ Form::open([ 'route' => ['product.destroy', $product->Id], 'method' => 'DELETE' ]) }}                    
                {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                {{ Form::close() }}
            </td>
            <td width="8%"><a class="btn btn-primary" href="{{ route('product.index') }}">Cancel</a></td>
        </tr>
    </thead>
</table>
<ul>
  <li>Naam: {{ $product->Name }}</li>
  <li>Prijs: {{ $product->Price }}</li>
  <li>Verzendkosten: {{ $product->ShippingCost }}</li>
  <li>Totaal rating: {{ $product->TotalRating }}</li>
  <li>Miniatuur: {{ $product->Thumbnail }}</li>
  <li>Afbeelding: {{ $product->Image }}</li>
  <li>Aanbiedingspercent: {{ $product->DiscountPercentage }}</li>
  <li>Stemmen: {{ $product->Votes }}</li>
  <li>Categorie: {{ $product->getCategoryName() }}</li>
  <li>Leverancier: {{ $product->getSupplierName() }}</li>
  <li>Beschrijving: {{ $product->Description }}</li>
</ul>
@endsection
