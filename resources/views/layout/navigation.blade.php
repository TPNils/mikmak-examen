<?php
function printActiveClass($key)
{
    if (isset($GLOBALS['nav.activePage']) && $GLOBALS['nav.activePage'] == $key)
        echo ' class="active"';
}
?>
<nav>
    <div id="nav-header">
        <a<?php printActiveClass("home"); ?> href="{{ URL::to('/') }}">Home</a>
    </div>
    <div id="nav-main">
        <ul>
            <li<?php printActiveClass("category"); ?>>
                <a href="{{ URL::to('category') }}">Category</a>
                <ul>
                    <li><a href="{{ URL::to('category') }}">Toon alle Categorieën</a></li>
                    <li><a href="{{ URL::to('category/create') }}">Category toevoegen</a></li>
                </ul>
            </li>
            <li<?php printActiveClass("country"); ?>>
                <a href="{{ URL::to('country') }}">Land</a>
                <ul>
                    <li><a href="{{ URL::to('country') }}">Toon alle Landen</a></li>
                    <li><a href="{{ URL::to('country/create') }}">Land toevoegen</a></li>
                </ul>
            </li>
            <li<?php printActiveClass("customer"); ?>>
                <a href="{{ URL::to('customer') }}">Klant</a>
                <ul>
                    <li><a href="{{ URL::to('customer') }}">Toon alle Klanten</a></li>
                    <li><a href="{{ URL::to('customer/create') }}">Klant toevoegen</a></li>
                </ul>
            </li>
            <li<?php printActiveClass("product"); ?>>
                <a href="{{ URL::to('product') }}">Product</a>
                <ul>
                    <li><a href="{{ URL::to('product') }}">Toon alle Producten</a></li>
                    <li><a href="{{ URL::to('product/create') }}">Product toevoegen</a></li>
                </ul>
            </li>
        </ul>
    </div>
    <div id="nav-footer">
        <span>MikMak</span>
    </div>
</nav>