<!DOCTYPE html>
<html>
<head>
    <title>@yield('title')</title>
    <link rel="icon" href="http://www.cvoantwerpen.be/uploads/tf/favicon.ico" type="image/x-ico; charset=binary">
    <link href="{{ secure_asset('css/main.css') }}" rel="stylesheet">
    <link href="{{ secure_asset('css/header-navigation.css') }}" rel="stylesheet">
    @yield('head')
</head>
<body>
    @yield('body-before')
    <header>
        @include('layout.navigation')
    </header>
    <main>
        @yield('main')
    </main>
    <footer>
        Made by: Nils Yseboodt
    </footer>
    @yield('body-after')
</body>
</html>
