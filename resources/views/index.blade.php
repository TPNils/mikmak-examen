<?php $GLOBALS['nav.activePage']="home"; ?>
@extends('layout.master')
@section('title')
Home
@endsection()

@section('head')
<title>Home</title>
<style type="text/css">
main{
    background-color: blue;
}
.floor{
    height: 100%;
    width: 100%;
    background-color: yellow;
}
.control-panel{
    height: 12%;
    width: 100%;
    background-color: red;
}
.show-room{
    height: 100%;
    width: 100%;
    background-color: green;
}
.tile{
    font-size: 2em;
    display: flex;
    flex-direction: column;
    justify-content: center;
    margin: auto;
    text-align: center;
    
    background-color: SteelBlue;
    color: white;
    
    height: 33.33%;
    width: 25%;
    border-right: solid white 2px;
    border-bottom: solid white 2px;
    float: left;
    cursor: pointer;
    position: relative;
    
    transition:font-size 1s;
}

.tile a{
    height: 100%;
    width: 100%;
    color: white;
    text-decoration: none;
    
    display: flex;
    flex-direction: column;
    justify-content: center;
    margin: auto;
    text-align: center;
}
.tile:hover{
    font-size: 2.5em;
}

</style>
@endsection()

@section('main')
<section class="floor">
    <article class="show-room">
        <!-- (div>(a>h2+p)+h1+p)*6 -->
        <div class="tile">
            <a href="{{ URL::to('category') }}"><h1>Category</h1></a>
        </div>
        <div class="tile">
            <a href="{{ URL::to('country') }}"><h1>Land</h1></a>
        </div>
        <div class="tile">
            <a href="{{ URL::to('customer') }}"><h1>Klant</h1></a>
        </div>
        <div class="tile">
            <a href="{{ URL::to('product') }}"><h1>Product</h1></a>
        </div>
        <div class="tile">
        </div>
        <div class="tile">
        </div>
        <div class="tile">
        </div>
        <div class="tile">
        </div>
        <div class="tile">
        </div>
        <div class="tile">
        </div>
        <div class="tile">
        </div>
        <div class="tile">
        </div>
    </article>
</section>
@endsection()