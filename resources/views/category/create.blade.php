<?php $GLOBALS['nav.activePage']="category"; ?>
@extends('layout.crud')

@section('title')
Maak een category aan
@endsection

@section('side-view')
@include('category.layout.aside')
@endsection()

@section('content')
{!! Form::open([
    'route' => 'category.store'
]) !!}

<table class="table" style="border: none;">
    <thead style="text-align: center">
        <tr>
            <td>Category</td>
            <td width="8%">{!! Form::submit('Create', ['class' => 'btn btn-primary']) !!}</a></td>
            <td width="8%"><a class="btn btn-primary" href="{{ route('category.index') }}">Cancel</a></td>
        </tr>
    </thead>
</table>

<div class="form-group">
    {!! Form::label('name', 'Name:', ['class' => 'control-label']) !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('description', 'Description:', ['class' => 'control-label']) !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>

{!! Form::close() !!}
@endsection()