<?php $GLOBALS['nav.activePage']="category"; ?>
@extends('layout.crud')

@section('title')
{{ $category->Name }}
@endsection

@section('side-view')
@include('category.layout.aside')
@endsection()

@section('content')
<table class="table" style="border: none;">
    <thead style="text-align: center">
        <tr>
            <td>Category</td>
            <td width="8%"><a class="btn btn-primary" href="{{ route('category.edit', $category->Id) }}">Update</a></td>
            <td width="8%"><a class="btn btn-primary" href="{{ route('category.create') }}">Nieuw</a></td>
            <td width="8%">
                {{ Form::open([ 'route' => ['category.destroy', $category->Id], 'method' => 'DELETE' ]) }}                    
                {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                {{ Form::close() }}
            </td>
            <td width="8%"><a class="btn btn-primary" href="{{ route('category.index') }}">Cancel</a></td>
        </tr>
    </thead>
</table>
<ul>
  <li>Naam: {{ $category->Name }}</li>
  <li>Beschrijving: {{ $category->Description }}</li>
</ul>
@endsection
