<?php $GLOBALS['nav.activePage']="customer"; ?>
@extends('layout.crud')

@section('title')
{{ $customer->NickName }}
@endsection

@section('side-view')
@include('customer.layout.aside')
@endsection()

@section('content')
<table class="table" style="border: none;">
    <thead style="text-align: center">
        <tr>
            <td>Klant</td>
            <td width="8%"><a class="btn btn-primary" href="{{ route('customer.edit', $customer->Id) }}">Update</a></td>
            <td width="8%"><a class="btn btn-primary" href="{{ route('customer.create') }}">Nieuw</a></td>
            <td width="8%">
                {{ Form::open([ 'route' => ['customer.destroy', $customer->Id], 'method' => 'DELETE' ]) }}                    
                {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                {{ Form::close() }}
            </td>
            <td width="8%"><a class="btn btn-primary" href="{{ route('customer.index') }}">Cancel</a></td>
        </tr>
    </thead>
</table>
<ul>
  <li>Roepnaam: {{ $customer->NickName }}</li>
  <li>Voornaam: {{ $customer->FirstName }}</li>
  <li>Familienaam: {{ $customer->LastName }}</li>
  <li>Adres 1: {{ $customer->Address1 }}</li>
  <li>Adres 2: {{ $customer->Address2 }}</li>
  <li>Stad: {{ $customer->City }}</li>
  <li>Regio: {{ $customer->Region }}</li>
  <li>Postcode: {{ $customer->PostalCode }}</li>
  <li>Land: {{ $customer->getCountryName() }}</li>
  <li>Telefoon: {{ $customer->Phone }}</li>
  <li>Mobieltje: {{ $customer->Mobile }}</li>
</ul>
@endsection
