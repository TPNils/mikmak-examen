<?php $GLOBALS['nav.activePage']="customer"; ?>
@extends('layout.master')

@section('head')
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
@endsection

@section('title')
Klant
@endsection

@section('main')
<table style="border: none; width: 100%">
    <thead style="text-align: center">
        <tr>
            <td><h1>Klanten</h1></td>
            <td width="8%"><a class="btn btn-primary" href="{{ route('customer.create') }}">Nieuw</a></td>
        </tr>
    </thead>
</table>
<table class="table-classic">
    <thead>
        <tr>
            <td width="20%">Roepnaam</td>
            <td>Voledige naam</td>
            <td width="10%">Select</td>
            <td width="10%">Update</td>
            <td width="10%">Delete</td>
        </tr>
    </thead>
    <tbody>
    @foreach($customers as $key => $value)
        <tr>
            <td>{{ $value->NickName }}</td>
            <td>{{ $value->LastName }} {{ $value->FirstName }}</td>
            <td style="text-align: center"><a href="{{ route('customer.show', $value->Id) }}" class="btn btn-primary">Select</a></td>
            <td style="text-align: center"><a href="{{ route('customer.edit', $value->Id) }}" class="btn btn-primary">Update</a></td>
            <td style="text-align: center">
            {!! Form::open([
                'route' => ['customer.destroy', $value->Id],
                'method' => 'DELETE'
            ]) !!}                    
            {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
            {{ Form::close() }}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
{{ $customers->links() }}
@endsection