<?php $GLOBALS['nav.activePage']="customer"; ?>
@extends('layout.crud')

@section('title')
Edit {{ $customer->Name }}
@endsection

@section('side-view')
@include('customer.layout.aside')
@endsection()

@section('content')
{!! Form::open([
    'route' => ['customer.update', $customer->Id],
    'method' => 'PUT'
]) !!}

<table class="table" style="border: none;">
    <thead style="text-align: center">
        <tr>
            <td>Klant</td>
            <td width="8%">{!! Form::submit('Edit', ['class' => 'btn btn-primary']) !!}</a></td>
            <td width="8%"><a class="btn btn-primary" href="{{ route('customer.index') }}">Cancel</a></td>
        </tr>
    </thead>
</table>

<div class="form-group">
    {!! Form::label('nickName', 'Roepnaam:', ['class' => 'control-label']) !!}
    {!! Form::text('nickName', $customer->NickName, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('firstName', 'Voornaam:', ['class' => 'control-label']) !!}
    {!! Form::text('firstName', $customer->FirstName, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('lastName', 'Familienaam:', ['class' => 'control-label']) !!}
    {!! Form::text('lastName', $customer->LastName, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('address1', 'Adres 1:', ['class' => 'control-label']) !!}
    {!! Form::text('address1', $customer->Address1, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('address2', 'Adres 2:', ['class' => 'control-label']) !!}
    {!! Form::text('address2', $customer->Address2, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('city', 'Stad:', ['class' => 'control-label']) !!}
    {!! Form::text('city', $customer->City, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('region', 'Regio:', ['class' => 'control-label']) !!}
    {!! Form::text('region', $customer->Region, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('postalCode', 'Postcode:', ['class' => 'control-label']) !!}
    {!! Form::text('postalCode', $customer->PostalCode, ['class' => 'form-control']) !!}
</div>



<div class="form-group">
    {!! Form::label('countryName', 'Land:', ['class' => 'control-label']) !!}
    {!! Form::text('countryName', $customer->getCountryName(), ['class' => 'form-control']) !!}
</div>



<div class="form-group">
    {!! Form::label('phone', 'Telefoon:', ['class' => 'control-label']) !!}
    {!! Form::text('phone', $customer->Phone, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('mobile', 'Mobieltje:', ['class' => 'control-label']) !!}
    {!! Form::text('mobile', $customer->Mobile, ['class' => 'form-control']) !!}
</div>

{!! Form::close() !!}
@endsection
