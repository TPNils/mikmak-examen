<table class="table-classic">
    <thead>
        <tr>
            <td width="15%">Select</td>
            <td>Roepnaam</td>
        </tr>
    </thead>
    <tbody>
    @foreach($customers as $key => $value)
        <tr>
            <td style="text-align: center"><a href="{{ route('customer.show', $value->Id) }}" class="btn btn-primary">Select</a></td>
            <td>{{ $value->NickName }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
{{ $customers->links() }}