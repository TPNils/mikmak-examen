<?php $GLOBALS['nav.activePage']="customer"; ?>
@extends('layout.crud')

@section('title')
Nieuwe klant
@endsection

@section('side-view')
@include('customer.layout.aside')
@endsection()

@section('content')
{!! Form::open([
    'route' => 'customer.store'
]) !!}

<table class="table" style="border: none;">
    <thead style="text-align: center">
        <tr>
            <td>Klant</td>
            <td width="8%">{!! Form::submit('Create', ['class' => 'btn btn-primary']) !!}</a></td>
            <td width="8%"><a class="btn btn-primary" href="{{ route('customer.index') }}">Cancel</a></td>
        </tr>
    </thead>
</table>

<div class="form-group">
    {!! Form::label('nickName', 'Roepnaam:', ['class' => 'control-label']) !!}
    {!! Form::text('nickName', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('firstName', 'Voornaam:', ['class' => 'control-label']) !!}
    {!! Form::text('firstName', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('lastName', 'Familienaam:', ['class' => 'control-label']) !!}
    {!! Form::text('lastName', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('address1', 'Adres 1:', ['class' => 'control-label']) !!}
    {!! Form::text('address1', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('address2', 'Adres 2:', ['class' => 'control-label']) !!}
    {!! Form::text('address2', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('city', 'Stad:', ['class' => 'control-label']) !!}
    {!! Form::text('city', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('region', 'Regio:', ['class' => 'control-label']) !!}
    {!! Form::text('region', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('postalCode', 'Postcode:', ['class' => 'control-label']) !!}
    {!! Form::text('postalCode', null, ['class' => 'form-control']) !!}
</div>



<div class="form-group">
    {!! Form::label('countryName', 'Land:', ['class' => 'control-label']) !!}
    {!! Form::text('countryName', null, ['class' => 'form-control']) !!}
</div>



<div class="form-group">
    {!! Form::label('phone', 'Telefoon:', ['class' => 'control-label']) !!}
    {!! Form::text('phone', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('mobile', 'Mobieltje:', ['class' => 'control-label']) !!}
    {!! Form::text('mobile', null, ['class' => 'form-control']) !!}
</div>

{!! Form::close() !!}
@endsection()