<?php $GLOBALS['nav.activePage']="country"; ?>
@extends('layout.crud')

@section('title')
{{ $country->Name }}
@endsection

@section('side-view')
@include('country.layout.aside')
@endsection()

@section('content')
<table class="table" style="border: none;">
    <thead style="text-align: center">
        <tr>
            <td>Land</td>
            <td width="8%"><a class="btn btn-primary" href="{{ route('country.edit', $country->Id) }}">Update</a></td>
            <td width="8%"><a class="btn btn-primary" href="{{ route('country.create') }}">Nieuw</a></td>
            <td width="8%">
                {{ Form::open([ 'route' => ['country.destroy', $country->Id], 'method' => 'DELETE' ]) }}                    
                {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                {{ Form::close() }}
            </td>
            <td width="8%"><a class="btn btn-primary" href="{{ route('country.index') }}">Cancel</a></td>
        </tr>
    </thead>
</table>
<ul>
  <li>Naam: {{ $country->Name }}</li>
  <li>Code: {{ $country->Code }}</li>
  <li>Breedtegraad: {{ $country->Latitude }}</li>
  <li>Lengtegraad: {{ $country->Longitude }}</li>
  <li>Verzendkosten: {{ $country->ShippingCostMultiplier }}</li>
</ul>
@endsection
