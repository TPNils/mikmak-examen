<table class="table-classic">
    <thead>
        <tr>
            <td width="15%">Select</td>
            <td>Naam</td>
        </tr>
    </thead>
    <tbody>
    @foreach($countries as $key => $value)
        <tr>
            <td style="text-align: center"><a href="{{ route('country.show', $value->Id) }}" class="btn btn-primary">Select</a></td>
            <td>{{ $value->Name }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
{{ $countries->links() }}