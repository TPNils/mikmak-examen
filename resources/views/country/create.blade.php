<?php $GLOBALS['nav.activePage']="country"; ?>
@extends('layout.crud')

@section('title')
Nieuw land
@endsection

@section('side-view')
@include('country.layout.aside')
@endsection()

@section('content')
{!! Form::open([
    'route' => 'country.store'
]) !!}

<table class="table" style="border: none;">
    <thead style="text-align: center">
        <tr>
            <td>Land</td>
            <td width="8%">{!! Form::submit('Create', ['class' => 'btn btn-primary']) !!}</a></td>
            <td width="8%"><a class="btn btn-primary" href="{{ route('country.index') }}">Cancel</a></td>
        </tr>
    </thead>
</table>

<div class="form-group">
    {!! Form::label('name', 'Name:', ['class' => 'control-label']) !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('code', 'Code:', ['class' => 'control-label']) !!}
    {!! Form::text('code', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('latitude', 'Breedtegraad:', ['class' => 'control-label']) !!}
    {!! Form::number('latitude', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('longitude', 'Lengtegraad:', ['class' => 'control-label']) !!}
    {!! Form::number('longitude', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('shippingCostMultiplier', 'Verzendkosten:', ['class' => 'control-label']) !!}
    {!! Form::number('shippingCostMultiplier', null, ['class' => 'form-control']) !!}
</div>

{!! Form::close() !!}
@endsection()