<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Product', function (Blueprint $table) {
            $table->increments('Id');
            $table->integer('IdSupplier')->unsigned();
            $table->integer('IdCategory')->unsigned();
            $table->string('Name', 255)->unique();
            $table->string('Description', 1024)->nullable();
            $table->decimal('Price', 6, 2);
            $table->float('ShippingCost')->nullable();
            $table->integer('TotalRating')->nullable();
            $table->string('Thumbnail', 255)->nullable();
            $table->string('Image', 255)->nullable();
            $table->float('DiscountPercentage')->nullable();
            $table->integer('Votes')->nullable();
            
            $table->foreign('IdSupplier')->references('Id')->on('Supplier');
            $table->foreign('IdCategory')->references('Id')->on('Category');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Product');
    }
}
