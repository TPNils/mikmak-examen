<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnitBasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('UnitBase', function (Blueprint $table) {
            $table->increments('Id');
            $table->string('Code', 2)->unique();
            $table->string('Name', 255)->unique();
            $table->string('Description', 1024)->nullable();
            $table->float('ShippingCostMultiplier')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('UnitBase');
    }
}
