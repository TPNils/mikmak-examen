<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuppliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Supplier', function (Blueprint $table) {
            $table->increments('Id');
            $table->integer('IdCountry')->unsigned();
            $table->string('Code', 10)->unique();
            $table->string('Name', 255)->unique();
            $table->string('Contact', 255)->nullable();
            $table->string('Address', 255)->nullable();
            $table->string('City', 255)->nullable();
            $table->string('Region', 80)->nullable();
            $table->string('PostalCode', 20)->nullable();
            $table->string('Phone', 40)->unique();
            $table->string('Mobile', 40)->unique();
            
            $table->foreign('IdCountry')->references('Id')->on('Country');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Supplier');
    }
}
