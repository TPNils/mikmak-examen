<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Customer', function (Blueprint $table) {
            $table->increments('Id');
            $table->string('NickName', 10);
            $table->string('FirstName', 255);
            $table->string('LastName', 255);
            $table->string('Address1', 255);
            $table->string('Address2', 255)->nullable();
            $table->string('City', 255);
            $table->string('Region', 80)->nullable();
            $table->string('PostalCode', 20);
            $table->integer('IdCountry')->unsigned();
            $table->string('Phone', 40)->unique();
            $table->string('Mobile', 40)->unique();
            
            $table->foreign('IdCountry')->references('Id')->on('Country');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Customer');
    }
}
