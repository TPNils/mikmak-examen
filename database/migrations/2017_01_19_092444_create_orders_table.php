<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Order', function (Blueprint $table) {
            $table->increments('Id');
            $table->dateTime('OrderDate');
            $table->dateTime('ShippingDate');
            $table->string('Comment', 255)->nullable();
            $table->integer('IdCustomer')->unsigned();
            $table->integer('IdShippingMethod')->unsigned();
            $table->integer('IdStatus')->unsigned();
            
            $table->foreign('IdCustomer')->references('Id')->on('Customer');
            $table->foreign('IdShippingMethod')->references('Id')->on('ShippingMethod');
            $table->foreign('IdStatus')->references('Id')->on('OrderStatus');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Order');
    }
}
