<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('OrderItem', function (Blueprint $table) {
            $table->increments('Id');
            $table->decimal('Quantity', 4, 2)->nullable();
            $table->integer('IdProduct')->unsigned();
            $table->integer('IdOrder')->unsigned();
            
            $table->foreign('IdProduct')->references('Id')->on('Product');
            $table->foreign('IdOrder')->references('Id')->on('Order');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('OrderItem');
    }
}
