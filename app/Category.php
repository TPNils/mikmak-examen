<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    /**
     * The primary key associated with the model.
     *
     * @var string
     */
    protected $primaryKey = "Id";
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'Category';
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    
    static public function getByName($name)
    {
        $categories = \App\Category::where('Name', $name)->get();
        if (isset($categories) && isset($categories[0]))
            return $categories[0];
        else
            return null;
    }
}
