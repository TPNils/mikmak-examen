<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    /**
     * The primary key associated with the model.
     *
     * @var string
     */
    protected $primaryKey = "Id";
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'Customer';
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    
    public function getCountryName()
    {
        $country = \App\Country::find($this->IdCountry);
        if (isset($country))
            return $country->Name;
        else null;
    }
}
