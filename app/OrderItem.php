<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    /**
     * The primary key associated with the model.
     *
     * @var string
     */
    protected $primaryKey = "Id";
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'OrderItem';
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
