<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // load the view and pass the nerds
        return view('customer.index', array('customers' => $this->getCustomers()));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('customer.create', array('customers' => $this->getCustomers()));
    }

    /**
     * Store a newly created resource in storage.
     * Credits to: wimvandooren
     *
     * @return Response
     */
    public function store(Request $request) {
        $this->validate($request, [
            'nickName' => 'required|max:10',
            'firstName' => 'required|max:255',
            'lastName' => 'required|max:255',
            'address1' => 'required|max:255',
            'address2' => 'max:255',
            'city' => 'required|max:255',
            'region' => 'max:80',
            'postalCode' => 'max:20',
            'phone' => 'max:40',
            'mobile' => 'max:40',
            'countryName' => 'required|max:255',
        ]);
        
        $customer = new Customer();
        $customer->NickName = empty($request->input('nickName')) ? null : $request->input('nickName');
        $customer->FirstName = empty($request->input('firstName')) ? null : $request->input('firstName');
        $customer->LastName = empty($request->input('lastName')) ? null : $request->input('lastName');
        $customer->Address1 = empty($request->input('address1')) ? null : $request->input('address1');
        $customer->Address2 = empty($request->input('address2')) ? null : $request->input('address2');
        $customer->City = empty($request->input('city')) ? null : $request->input('city');
        $customer->Region = empty($request->input('region')) ? null : $request->input('region');
        $customer->PostalCode = empty($request->input('postalCode')) ? null : $request->input('postalCode');
        $customer->Phone = empty($request->input('phone')) ? null : $request->input('phone');
        $customer->Mobile = empty($request->input('mobile')) ? null : $request->input('mobile');
        
        $country = \App\Country::getByName($request->input('countryName'));
        $customer->IdCountry = isset($country) ? $country->Id : null;
        
        $country = \App\Country::getByName($request->input('countryName'));
        $customer->IdCountry = isset($country) ? $country->Id : null;
        
        $customer->save();
        return redirect('/customer');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $customer = Customer::find($id);
        return view('customer.show', array('customer' => $customer, 'customers' => $this->getCustomers()));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $customer = Customer::find($id);
        return view('customer.edit', array('customer' => $customer, 'customers' => $this->getCustomers()));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nickName' => 'required|max:10',
            'firstName' => 'required|max:255',
            'lastName' => 'required|max:255',
            'address1' => 'required|max:255',
            'address2' => 'required|max:255',
            'city' => 'required|max:255',
            'region' => 'max:80',
            'postalCode' => 'max:20',
            'phone' => 'required|max:40',
            'mobile' => 'required|max:40',
            'countryName' => 'required|max:255',
        ]);
        
        $customer = Customer::find($id);
        if ($customer == null)
        {
            abort(404);
        }
        
        $customer->NickName = empty($request->input('nickName')) ? null : $request->input('nickName');
        $customer->FirstName = empty($request->input('firstName')) ? null : $request->input('firstName');
        $customer->LastName = empty($request->input('lastName')) ? null : $request->input('lastName');
        $customer->Address1 = empty($request->input('address1')) ? null : $request->input('address1');
        $customer->Address2 = empty($request->input('address2')) ? null : $request->input('address2');
        $customer->City = empty($request->input('city')) ? null : $request->input('city');
        $customer->Region = empty($request->input('region')) ? null : $request->input('region');
        $customer->PostalCode = empty($request->input('postalCode')) ? null : $request->input('postalCode');
        $customer->Phone = empty($request->input('phone')) ? null : $request->input('phone');
        $customer->Mobile = empty($request->input('mobile')) ? null : $request->input('mobile');
        
        $country = \App\Country::getByName($request->input('countryName'));
        $customer->IdCountry = isset($country) ? $country->Id : null;
        
        $customer->save();
        
        return redirect('/customer');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $country = Customer::find($id);
        $country->delete();
        
        // get all the categories
        return redirect('/customer');
    }
    
    private function getCustomers()
    {
        return \Illuminate\Support\Facades\DB::table('Customer')->orderBy('LastName')->orderBy('FirstName')->paginate(10);
    }
}
