<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Country;

class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // load the view and pass the nerds
        return view('country.index', array('countries' => $this->getCountries()));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('country.create', array('countries' => $this->getCountries()));
    }

    /**
     * Store a newly created resource in storage.
     * Credits to: wimvandooren
     *
     * @return Response
     */
    public function store(Request $request) {
        $this->validate($request, [
            'code' => 'bail|required|unique:Country|max:2',
            'name' => 'bail|required|unique:Country|max:255',
            'latitude' => 'regex:/^[0-9]+(\.[0-9]{1,3})?$/',
            'longitude' => 'regex:/^[0-9]+(\.[0-9]{1,3})?$/',
            'shippingCostMultiplier' => 'regex:/^[0-9]+(\.[0-9]{1,3})?$/',
        ]);
        
        $country = new Country();
        $country->Name = $request->input('name');
        $country->Code = $request->input('code');
        $country->Latitude = empty($request->input('latitude')) ? null : $request->input('latitude');
        $country->Longitude = empty($request->input('longitude')) ? null : $request->input('longitude');
        $country->ShippingCostMultiplier = empty($request->input('shippingCostMultiplier')) ? null : $request->input('shippingCostMultiplier');
        $country->save();
        
        return redirect('/country');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $country = Country::find($id);
        return view('country.show', array('country' => $country, 'countries' => $this->getCountries()));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $country = Country::find($id);
        return view('country.edit', array('country' => $country, 'countries' => $this->getCountries()));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'code' => "bail|required|unique:Country,Id,$id|max:2",
            'name' => "bail|required|unique:Country,Id,$id|max:255",
            'latitude' => 'regex:/^[0-9]+(\.[0-9]{1,3})?$/',
            'longitude' => 'regex:/^[0-9]+(\.[0-9]{1,3})?$/',
            'shippingCostMultiplier' => 'regex:/^[0-9]+(\.[0-9]{1,3})?$/',
        ]);
        
        $country = Country::find($id);
        if ($country == null)
        {
            abort(404);
        }
        
        $country->Name = $request->input('name');
        $country->Code = $request->input('code');
        $country->Latitude = empty($request->input('latitude')) ? null : $request->input('latitude');
        $country->Longitude = empty($request->input('longitude')) ? null : $request->input('longitude');
        $country->ShippingCostMultiplier = empty($request->input('shippingCostMultiplier')) ? null : $request->input('shippingCostMultiplier');
        $country->save();
        
        return redirect('/country');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $country = Country::find($id);
        $country->delete();
        
        // get all the categories
        return redirect('/country');
    }
    
    private function getCountries()
    {
        //TODO handle swiching pages
        return \Illuminate\Support\Facades\DB::table('Country')->orderBy('Name')->paginate(10);
    }
}
