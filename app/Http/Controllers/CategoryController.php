<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // load the view and pass the nerds
        return view('category.index', array('categories' => $this->getCatagories()));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('category.create', array('categories' => $this->getCatagories()));
    }

    /**
     * Store a newly created resource in storage.
     * Credits to: wimvandooren
     *
     * @return Response
     */
    public function store(Request $request) {
        $this->validate($request, [
            'name' => 'bail|required|unique:Category|max:255',
            'description' => 'max:1024',
        ]);
        
        $category = new Category();
        $category->Name = $request->input('name');
        $category->Description = $request->input('description');
        $category->save();
        
        return redirect('/category');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $category = Category::find($id);
        return view('category.show', array('category' => $category, 'categories' => $this->getCatagories()));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $category = Category::find($id);
        return view('category.edit', array('category' => $category, 'categories' => $this->getCatagories()));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => "bail|required|unique:Category,Id,$id|max:255",
            'description' => 'max:1024',
        ]);
        
        $category = Category::find($id);
        if ($category == null)
        {
            abort(404);
        }
        
        $category->Name = $request->input('name');
        $category->Description = $request->input('description');
        $category->save();
        
        return redirect('/category');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);
        $category->delete();
        
        // get all the categories
        return redirect('/category');
    }
    
    private function getCatagories()
    {
        //TODO handle swiching pages
        return \Illuminate\Support\Facades\DB::table('Category')->orderBy('Name')->paginate(10);
    }
}
