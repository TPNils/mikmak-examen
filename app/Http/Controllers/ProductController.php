<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        // load the view and pass the nerds
        return view('product.index', array('products' => $this->getProducts()));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('product.create', array('products' => $this->getProducts()));
    }

    /**
     * Store a newly created resource in storage.
     * Credits to: wimvandooren
     *
     * @return Response
     */
    public function store(Request $request) {
        $this->validate($request, [
            'name' => 'required|unique:Product|max:255',
            'description' => 'max:1024',
            'price' => 'regex:/^[0-9]+(\.[0-9][0-9]?)?$/',
            'shippingCost' => 'regex:/^[0-9]+(\.[0-9][0-9]?)?$/',
            'thumbnail' => 'max:255',
            'image' => 'max:255',
            'discountPercentage' => 'regex:/^[0-9]+(\.[0-9][0-9]?)?$/',
            'categoryName' => 'required|max:255',
        ]);
        
        $product = new Product();
        $product->Name = empty($request->input('name')) ? null : $request->input('name');
        $product->Description = empty($request->input('description')) ? null : $request->input('description');
        $product->Price = empty($request->input('price')) ? null : $request->input('price');
        $product->ShippingCost = empty($request->input('shippingCost')) ? null : $request->input('shippingCost');
        $product->Thumbnail = empty($request->input('thumbnail')) ? null : $request->input('thumbnail');
        $product->Image = empty($request->input('image')) ? null : $request->input('image');
        $product->DiscountPercentage = empty($request->input('discountPercentage')) ? null : $request->input('discountPercentage');
        $product->IdSupplier = 1;
        
        $category = \App\Category::getByName($request->input('categoryName'));
        $product->IdCategory = isset($category) ? $category->Id : null;
        
        $product->save();
        return redirect('/product');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $product = Product::find($id);
        return view('product.show', array('product' => $product, 'products' => $this->getProducts()));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        return view('product.edit', array('product' => $product, 'products' => $this->getProducts()));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => "required|unique:Product,Id,$id|max:255",
            'description' => 'max:1024',
            'price' => 'regex:/^[0-9]+(\.[0-9][0-9]?)?$/',
            'shippingCost' => 'regex:/^[0-9]+(\.[0-9][0-9]?)?$/',
            'thumbnail' => 'max:255',
            'image' => 'max:255',
            'discountPercentage' => 'regex:/^[0-9]+(\.[0-9][0-9]?)?$/',
            'categoryName' => 'required|max:255',
        ]);
        
        $product = Product::find($id);
        if ($product == null)
        {
            abort(404);
        }
        
        $product->Name = empty($request->input('name')) ? null : $request->input('name');
        $product->Description = empty($request->input('description')) ? null : $request->input('description');
        $product->Price = empty($request->input('price')) ? null : $request->input('price');
        $product->ShippingCost = empty($request->input('shippingCost')) ? null : $request->input('shippingCost');
        $product->Thumbnail = empty($request->input('thumbnail')) ? null : $request->input('thumbnail');
        $product->Image = empty($request->input('image')) ? null : $request->input('image');
        $product->DiscountPercentage = empty($request->input('discountPercentage')) ? null : $request->input('discountPercentage');
        $product->IdSupplier = 1;
        
        $category = \App\Category::getByName($request->input('categoryName'));
        $product->IdCategory = isset($category) ? $category->Id : null;
        
        $product->save();
        
        return redirect('/product');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        $product->delete();
        
        // get all the categories
        return redirect('/product');
    }
    
    private function getProducts()
    {
        return \Illuminate\Support\Facades\DB::table('Product')->orderBy('Name')->paginate(10);
    }
}
