<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    /**
     * The primary key associated with the model.
     *
     * @var string
     */
    protected $primaryKey = "Id";
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'Product';
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    
    public function getCategoryName()
    {
        $category = \App\Category::find($this->IdCategory);
        if (isset($category))
            return $category->Name;
        else null;
    }
    
    public function getSupplierName()
    {
        $supplier = \App\Supplier::find($this->IdSupplier);
        if (isset($supplier))
            return $supplier->Name;
        else null;
    }
}
