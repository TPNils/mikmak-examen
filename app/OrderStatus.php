<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderStatus extends Model
{
    /**
     * The primary key associated with the model.
     *
     * @var string
     */
    protected $primaryKey = "Id";
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'OrderStatus';
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
