<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    /**
     * The primary key associated with the model.
     *
     * @var string
     */
    protected $primaryKey = "Id";
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'Country';
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    
    static public function getByName($name)
    {
        $countries = \App\Country::where('Name', $name)->get();
        if (isset($countries) && isset($countries[0]))
            return $countries[0];
        else
            return null;
    }
}
