<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::resource('category', 'CategoryController');
Route::resource('country', 'CountryController');
Route::resource('customer', 'CustomerController');
Route::resource('product', 'ProductController');

Route::get('/', function () {
    return view('index');
});
